// We have a database consisting of a JavaScript object of books and their authors:
const DB = {
  books: [
    {
      id: 1,
      title: "The master algorithm",
      author: 1
    },
    {
      id: 2,
      title: "On intelligence",
      author: 3
    },
    {
      id: 3,
      title: "Superintelligence",
      author: 2
    },
    {
      id: 4,
      title: "How to create a mind",
      author: 4
    }
  ],
  authors: [
    {
      id: 1,
      name: "Pedro Domingos",
      wiki:
        "https://en.wikipedia.org/w/api.php?action=parse&page=Pedro_Domingos",
      similar: [2]
    },
    {
      id: 2,
      name: "Nick Bostrom",
      wiki: "https://en.wikipedia.org/w/api.php?action=parse&page=Nick_Bostrom",
      similar: [1, 3]
    },
    {
      id: 3,
      name: "Jeff Hawkins",
      wiki: "https://en.wikipedia.org/w/api.php?action=parse&page=Jeff_Hawkins",
      similar: [2]
    },
    {
      id: 4,
      name: "Ray Kurzweil",
      wiki: "https://en.wikipedia.org/w/api.php?action=parse&page=Ray_Kurzweil",
      similar: [3, 1]
    }
  ]
};

// Write a function which is able to parse a query like:
const query = {
  books: {
    3: {
      id: true,
      title: true,
      author: {
        name: true,
        photo: true
      }
    },
    1: {
      id: true,
      title: true,
      author: {
        name: true,
        similar: {
          name: true,
          books: {
            title: true
          }
        }
      }
    }
  },
  authors: {
    3: {
      id: true,
      name: true,
      similar: {
        name: true,
        similar: {
          name: true,
          wiki: {
            parse: {
              text: true
            }
          }
        }
      }
    }
  }
};
// And return a promise with the resolved information like:
/*
result = {
  books: [
    {
      id: 3,
      title: ...,
      author: {
        ...
      }
    },
    {
      id: 1,
      title: ...,
      author: {
        ...
      }
    }
  ],
  authors: [
    {
      id: 3,
      name: ...,
      similar: {
        ...
      }
    }
  ]
}
*/
