// We have a database consisting of a JavaScript object of books and their authors:
const DB = {
  books: [
    {
      id: 1,
      title: "The master algorithm",
      author: 1
    },
    {
      id: 2,
      title: "On intelligence",
      author: 3
    },
    {
      id: 3,
      title: "Superintelligence",
      author: 2
    },
    {
      id: 4,
      title: "How to create a mind",
      author: 4
    }
  ],
  authors: [
    {
      id: 1,
      name: "Pedro Domingos",
      wiki:
        "https://en.wikipedia.org/w/api.php?action=parse&page=Pedro_Domingos",
      similar: [2]
    },
    {
      id: 2,
      name: "Nick Bostrom",
      wiki: "https://en.wikipedia.org/w/api.php?action=parse&page=Nick_Bostrom",
      similar: [1, 3]
    },
    {
      id: 3,
      name: "Jeff Hawkins",
      wiki: "https://en.wikipedia.org/w/api.php?action=parse&page=Jeff_Hawkins",
      similar: [2]
    },
    {
      id: 4,
      name: "Ray Kurzweil",
      wiki: "https://en.wikipedia.org/w/api.php?action=parse&page=Ray_Kurzweil",
      similar: [3, 1]
    }
  ]
};

// Write a function which is able to parse a query like:
const query = {
  books: {
    3: {
      id: true,
      title: true,
      author: {
        name: true,
        photo: true
      }
    },
    1: {
      id: true,
      title: true,
      author: {
        name: true,
        similar: {
          name: true,
          books: {
            title: true
          }
        }
      }
    }
  },
  authors: {
    3: {
      id: true,
      name: true,
      similar: {
        name: true,
        similar: {
          name: true,
          wiki: {
            parse: {
              text: true
            }
          }
        }
      }
    }
  }
};

//My code begins here
const util = require('util');
const rp = require('request-promise-native');


function resolve_query(query) {
  let books = [];

  if (query.books) {
    //Map over books and make a promise for each
    books = Object.keys(query.books).map(book => {
      return resolve_book(book, query.books[book]);
    });
  }

  let authors = [];

  if (query.authors) {
    //Map over authors and make a promise for each
    authors = Object.keys(query.authors).map(author => {
      return resolve_author(author, query.authors[author]);
    });
  }

  return Promise.all([Promise.all(books), Promise.all(authors)]).then((things) => {
    return {books: things[0], authors: things[1]};
  });
}

function resolve_book(id, query) {
  //If this was a database, it would be resolved async
  return Promise.resolve(DB.books[id - 1]).then(data => {
    let result = {};

    for (const key of Object.keys(data)) {
      if (query[key]) {
        // We could check if the value of query[key] is true but
        // the only query we have only has true and objects
        result[key] = data[key];
      }
    }

    if (query.author) {
      return resolve_author(data.author, query.author).then(author => {
        result.author = author;
        return result;
      })
    } else {
      return Promise.resolve(result);
    }
  })
}

function resolve_author(id, query) {
  return Promise.resolve(DB.authors[id - 1]).then(data => {
    let result = {};
    
    for (const key of Object.keys(data)) {
      if (query[key]) {
        // We could check if the value of query[key] is true but
        // the only query we have only has true and objects
        result[key] = data[key];
      }
    }

    let similar = [];
    if (query.similar) {
      similar = data.similar.map(author => {
        return resolve_author(author, query.similar);
      });
    }

    similar = Promise.all(similar);

    let wiki;
    if (query.wiki) {
      wiki = resolve_wiki(data.wiki, query.wiki)
    }

    return Promise.all([similar, wiki]).then((things) => {
      if (query.similar) {result.similar = things[0];}
      if (query.wiki) {result.wiki = things[1];}
      return result;
    });
  })
}

function resolve_wiki(url, query) {
  return rp(url + "&format=json").then(JSON.parse).then(function(data) {
    return parse(data, query);
  });
}

function parse(data, query) {
  let result = {};

  for (const key of Object.keys(query)) {
    if (query[key] === true) {
      result[key] = data[key];
    } else {
      result[key] = parse(data[key], query[key]);
    }
  }

  return result;
}

resolve_query(query).then(console.log);

// And return a promise with the resolved information like:
/*
result = {
  books: [
    {
      id: 3,
      title: ...,
      author: {
        ...
      }
    },
    {
      id: 1,
      title: ...,
      author: {
        ...
      }
    }
  ],
  authors: [
    {
      id: 3,
      name: ...,
      similar: {
        ...
      }
    }
  ]
}
*/
